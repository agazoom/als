<?php 
/* Attorneys Functions*/
/* setup theme panel */
require_once ('admin/index.php');
/* shortcodes */
include("shortcode.php"); 
if ( ! isset( $content_width ) )
	$content_width = 640;
/* add custom sizes to WP uploader */
add_filter('image_size_names_choose', 'my_image_sizes');
function my_image_sizes($sizes) {
        $addsizes = array(
                "awards" => "Awards"
                );
        $newsizes = array_merge($sizes, $addsizes);
        return $newsizes;
}
/** Tell WordPress to run attorneys_setup() when the 'after_setup_theme' hook is run. */
add_action( 'after_setup_theme', 'attorneys_setup' );

if ( ! function_exists( 'attorneys_setup' ) ):
function attorneys_setup() {

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// Post Format support. You can also use the legacy "gallery" or "asides" (note the plural) categories.
	add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );

	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );

	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );

	// Make theme available for translation
	// Translations can be filed in the /languages/ directory
	load_theme_textdomain( 'attorneys', get_template_directory() . '/languages' );

	$locale = get_locale();
	$locale_file = get_template_directory() . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'attorneys' ),
		'mobile-menu' => __( 'Mobile Menu', 'attorneys' ),
	) );

	// This theme allows users to set a custom background
	add_custom_background();

	// Your changeable header business starts here
	if ( ! defined( 'HEADER_TEXTCOLOR' ) )
		define( 'HEADER_TEXTCOLOR', '' );

	// No CSS, just IMG call. The %s is a placeholder for the theme template directory URI.
	if ( ! defined( 'HEADER_IMAGE' ) )
		define( 'HEADER_IMAGE', '%s/images/headers/path.jpg' );

	// The height and width of your custom header. You can hook into the theme's own filters to change these values.
	// Add a filter to attorneys_header_image_width and attorneys_header_image_height to change these values.
	define( 'HEADER_IMAGE_WIDTH', apply_filters( 'attorneys_header_image_width', 940 ) );
	define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'attorneys_header_image_height', 198 ) );

	// We'll be using post thumbnails for custom header images on posts and pages.
	// We want them to be 940 pixels wide by 198 pixels tall.
	// Larger images will be auto-cropped to fit, smaller ones will be ignored. See header.php.
	set_post_thumbnail_size( HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true );

	// Don't support text inside the header image.
	if ( ! defined( 'NO_HEADER_TEXT' ) )
		define( 'NO_HEADER_TEXT', true );

	// Add a way for the custom header to be styled in the admin panel that controls
	// custom headers. See attorneys_admin_header_style(), below.
	add_custom_image_header( '', 'attorneys_admin_header_style' );

	// ... and thus ends the changeable header business.

	// Default custom headers packaged with the theme. %s is a placeholder for the theme template directory URI.
	register_default_headers( array(
		'berries' => array(
			'url' => '%s/images/headers/berries.jpg',
			'thumbnail_url' => '%s/images/headers/berries-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Berries', 'attorneys' )
		),
		'cherryblossom' => array(
			'url' => '%s/images/headers/cherryblossoms.jpg',
			'thumbnail_url' => '%s/images/headers/cherryblossoms-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Cherry Blossoms', 'attorneys' )
		),
		'concave' => array(
			'url' => '%s/images/headers/concave.jpg',
			'thumbnail_url' => '%s/images/headers/concave-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Concave', 'attorneys' )
		),
		'fern' => array(
			'url' => '%s/images/headers/fern.jpg',
			'thumbnail_url' => '%s/images/headers/fern-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Fern', 'attorneys' )
		),
		'forestfloor' => array(
			'url' => '%s/images/headers/forestfloor.jpg',
			'thumbnail_url' => '%s/images/headers/forestfloor-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Forest Floor', 'attorneys' )
		),
		'inkwell' => array(
			'url' => '%s/images/headers/inkwell.jpg',
			'thumbnail_url' => '%s/images/headers/inkwell-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Inkwell', 'attorneys' )
		),
		'path' => array(
			'url' => '%s/images/headers/path.jpg',
			'thumbnail_url' => '%s/images/headers/path-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Path', 'attorneys' )
		),
		'sunset' => array(
			'url' => '%s/images/headers/sunset.jpg',
			'thumbnail_url' => '%s/images/headers/sunset-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Sunset', 'attorneys' )
		)
	) );
}
endif;

if ( ! function_exists( 'attorneys_admin_header_style' ) ) :
function attorneys_admin_header_style() {
?>
<style type="text/css">
/* Shows the same border as on front end */
#headimg {
	border-bottom: 1px solid #000;
	border-top: 4px solid #000;
}
/* If NO_HEADER_TEXT is false, you would style the text with these selectors:
	#headimg #name { }
	#headimg #desc { }
*/
</style>
<?php
}
endif;

function attorneys_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'attorneys_page_menu_args' );

function attorneys_excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'attorneys_excerpt_length' );

function attorneys_continue_reading_link() {
	return ' <a href="'. get_permalink() . '">' . __( 'more', 'attorneys' ) . '</a>';
}
function attorneys_auto_excerpt_more( $more ) {
	return ' &hellip;' . attorneys_continue_reading_link();
}
add_filter( 'excerpt_more', 'attorneys_auto_excerpt_more' );
function attorneys_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= attorneys_continue_reading_link();
	}
	return $output;
}
#add_filter( 'get_the_excerpt', 'attorneys_custom_excerpt_more' );
add_filter( 'use_default_gallery_style', '__return_false' );
function attorneys_remove_gallery_css( $css ) {
	return preg_replace( "#<style type='text/css'>(.*?)</style>#s", '', $css );
}
// Backwards compatibility with WordPress 3.0.
if ( version_compare( $GLOBALS['wp_version'], '3.1', '<' ) )
	add_filter( 'gallery_style', 'attorneys_remove_gallery_css' );

if ( ! function_exists( 'attorneys_comment' ) ) :
function attorneys_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li>
		
		<div class="image">
			<?php echo get_avatar( $comment, 75 ); ?>
			
		</div><!-- .image -->
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'attorneys' ); ?></em>
			<br />
		<?php endif; ?>

		<h5><span><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
			<?php
				/* translators: 1: date, 2: time */
				echo comment_author();
			?>
			</a>
		</span> on <span class="othr"><?php printf( __( '%1$s at %2$s', 'attorneys' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'attorneys' ), ' ' );?></span></h5><!-- .comment-meta .commentmetadata -->

		<p><?php echo $comment->comment_content; ?>
		
		<strong>
			<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		</strong></p>
	</li>

	<?php
			break;
	case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'attorneys' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( '(Edit)', 'attorneys' ), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}
endif;
function attorneys_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Pages Sidebar', 'attorneys' ),
		'id' => 'primary-widget-area',
		'description' => __( 'The primary widget area for all pages', 'attorneys' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
		register_sidebar( array(
		'name' => __( 'Blog Sidebar', 'attorneys' ),
		'id' => 'blog-widgets',
		'description' => __( 'The primary widget area for blog page', 'attorneys' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
		register_sidebar( array(
		'name' => __( 'Footer Widgets', 'attorneys' ),
		'id' => 'footer-widgets',
		'description' => __( 'Footer widget area', 'attorneys' ),
		'before_widget' => '<div class="fwidget">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
}
/** Register sidebars by running attorneys_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'attorneys_widgets_init' );
function attorneys_remove_recent_comments_style() {
	add_filter( 'show_recent_comments_widget_style', '__return_false' );
}
add_action( 'widgets_init', 'attorneys_remove_recent_comments_style' );

if ( ! function_exists( 'attorneys_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 *
 * @since Twenty Ten 1.0
 */
function attorneys_posted_on() {
	printf( __( '<span class="%1$s">Posted on</span> %2$s <span class="meta-sep">by</span> %3$s', 'attorneys' ),
		'meta-prep meta-prep-author',
		sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="entry-date">%3$s</span></a>',
			get_permalink(),
			esc_attr( get_the_time() ),
			get_the_date()
		),
		sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s">%3$s</a></span>',
			get_author_posts_url( get_the_author_meta( 'ID' ) ),
			esc_attr( sprintf( __( 'View all posts by %s', 'attorneys' ), get_the_author() ) ),
			get_the_author()
		)
	);
}
endif;

if ( ! function_exists( 'attorneys_posted_in' ) ) :
function attorneys_posted_in() {
	// Retrieves tag list of current post, separated by commas.
	$tag_list = get_the_tag_list( '', ', ' );
	if ( $tag_list ) {
		$posted_in = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'attorneys' );
	} elseif ( is_object_in_taxonomy( get_post_type(), 'category' ) ) {
		$posted_in = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'attorneys' );
	} else {
		$posted_in = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'attorneys' );
	}
	// Prints the string, replacing the placeholders.
	printf(
		$posted_in,
		get_the_category_list( ', ' ),
		$tag_list,
		get_permalink(),
		the_title_attribute( 'echo=0' )
	);
}
endif;


/* Button Shortcode */
function button_shortcode( $atts,$content = null) {
   extract( shortcode_atts( array(      
      'style' => 'none',
	  'url' => 'none',
	  	  
      ), $atts ) );
 
   return '<a href="'.$atts[url].'" class="'.$atts[style].'"><span>' . $content . '</span></a>';
}
add_shortcode('button', 'button_shortcode');
/* End of Button Shortcode */

/* Message Shortcode */
function message_shortcode( $atts,$content = null) {
   extract( shortcode_atts( array(      
      'style' => 'none'	  	  	  
      ), $atts ) );
 
   return '<p>' . $content . '</p>';
}
add_shortcode('message', 'message_shortcode');
/* End of Message Shortcode */
function table_func( $atts ) {
 extract( shortcode_atts( array(
 'cols' => 'none',
 'data' => 'none',
 'style' => 'none',
 ), $atts ) );
 $cols = explode(',',$cols);
 $data = explode(',',$data);
 $total = count($cols);
 
$output = "<table style='$atts[style]'><tr>";
 foreach($cols as $col):
 $output .= "<td> {$col} </td>";
 endforeach;
 
$output .= "</tr>";
 
$counter = 1;
 foreach($data as $datum):
 $output .= "<td> {$datum} </td>";
 if($counter%$total==0):
 $output .= "</tr>";
 endif;
 $counter++;
 endforeach;
 $output .="</table>";
 
 return $output;
 
}
add_shortcode( 'table ', 'table_func' );
function print_excerpt($length) { // Max excerpt length. Length is set in characters
	global $post;
		$text = get_the_excerpt();
		$tags = array("<p>", "</p>");
  $text = str_replace($tags, "", $text);
		//$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]>', $text);
	$text = strip_shortcodes($text); // optional, recommended
	$text = strip_tags($text); // use ' $text = strip_tags($text,'<p><a>'); ' if you want to keep some tags

	$text = substr($text,0,$length);
	$excerpt = reverse_strrchr($text, '.', 1);
	if( $excerpt ) {
		echo apply_filters('the_excerpt',$excerpt);
	} else {
		echo apply_filters('the_excerpt',$text);
	}
}
function reverse_strrchr($haystack, $needle, $trail) {
    return strrpos($haystack, $needle) ? substr($haystack, 0, strrpos($haystack, $needle) + $trail) : false;
}
function the_breadcrumb() {
	if (!is_home()) {
		echo '<a href="';
		echo home_url();
		echo '">';
		echo 'Home';
		echo "</a>  ";
		if (is_category() || is_single()) {
			the_category('title_li=');
			if (is_single()) {
				echo "  ";
				the_title();
			}
		} elseif (is_page()) {
			echo the_title();
		}
	}
}

if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
        set_post_thumbnail_size( 150, 150 ); // default Post Thumbnail dimensions
}

if ( function_exists( 'add_image_size' ) ) {

        add_image_size( 'sidebar-thumb', 73, 76, true ); //(cropped)
		add_image_size( 'slider', 670,320, true );
		add_image_size( 'blog', 623,248, true );
		add_image_size( 'slider', 670,320, true );
				add_image_size( 'attorney', 306,187, true );
				add_image_size( 'attorney2', 241,187, true );
					add_image_size( 'book', 105,133, true );
      add_image_size( 'home', 200,168, true );
	        add_image_size( 'awards', 200,88, true );
}
/* register post type for attorneys */
	add_action( 'init', 'create_post_type' );
function create_post_type() {
	register_post_type( 'attorneys',
		array(
			'labels' => array(
				'name' => 'Attorneys',
				'singular_name' => 'Attorney'
			),
		'public' => true,
		'has_archive' => true,
		'supports' => array('title','editor','thumbnail')
		)
	);
		register_post_type( 'testimonials',
		array(
			'labels' => array(
				'name' => 'Testimonials',
				'singular_name' => 'Testimonial'
			),
		'public' => true,
		'has_archive' => true,
				'supports' => array('title','editor','thumbnail','custom-fields')
		)
	);
			register_post_type( 'areasofpractice',
		array(
			'labels' => array(
				'name' => 'Areas Of Practice',
				'singular_name' => 'Area Of Practice'
			),
		'public' => true,
		'has_archive' => true,
				'supports' => array('title','editor','thumbnail','custom-fields')
		)
	);
				register_post_type( 'books',
		array(
			'labels' => array(
				'name' => 'Books',
				'singular_name' => 'Book'
			),
		'public' => true,
		'has_archive' => true,
				'supports' => array('title','editor','thumbnail','custom-fields')
		)
	);
}
/* add designation for testimonials */
$prefix = 'dbt_';

$meta_box = array(
	'id' => 'my-meta-box',
	'title' => 'Designation',
	'page' => 'testimonials',
	'context' => 'normal',
	'priority' => 'high',
	'fields' => array(
		array(
			'name' => 'Add designation',
			'desc' => '',
			'id' => $prefix . 'designation',
			'type' => 'text',
			'std' => ''
		)
	)
);

add_action('admin_menu', 'mytheme_add_box');

// Add meta box
function mytheme_add_box() {
	global $meta_box;
	
	add_meta_box($meta_box['id'], $meta_box['title'], 'mytheme_show_box', $meta_box['page'], $meta_box['context'], $meta_box['priority']);
}

// Callback function to show fields in meta box
function mytheme_show_box() {
	global $meta_box, $post;
	
	// Use nonce for verification
	echo '<input type="hidden" name="mytheme_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';
	
	echo '<table class="form-table">';

	foreach ($meta_box['fields'] as $field) {
		// get current post meta data
		$meta = get_post_meta($post->ID, $field['id'], true);
		
		echo '<tr>',
				'<th style="width:20%"><label for="', $field['id'], '">', $field['name'], '</label></th>',
				'<td>';
		switch ($field['type']) {
			case 'text':
				echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />',
					'<br />', $field['desc'];
				break;
			case 'textarea':
				echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="4" style="width:97%">', $meta ? $meta : $field['std'], '</textarea>',
					'<br />', $field['desc'];
				break;
			case 'select':
				echo '<select name="', $field['id'], '" id="', $field['id'], '">';
				foreach ($field['options'] as $option) {
					echo '<option', $meta == $option ? ' selected="selected"' : '', '>', $option, '</option>';
				}
				echo '</select>';
				break;
			case 'radio':
				foreach ($field['options'] as $option) {
					echo '<input type="radio" name="', $field['id'], '" value="', $option['value'], '"', $meta == $option['value'] ? ' checked="checked"' : '', ' />', $option['name'];
				}
				break;
			case 'checkbox':
				echo '<input type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />';
				break;
		}
		echo 	'<td>',
			'</tr>';
	}
	
	echo '</table>';
}

add_action('save_post', 'mytheme_save_data');

// Save data from meta box
function mytheme_save_data($post_id) {
	global $meta_box;
	
	// verify nonce
	if (!wp_verify_nonce($_POST['mytheme_meta_box_nonce'], basename(__FILE__))) {
		return $post_id;
	}

	// check autosave
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		return $post_id;
	}

	// check permissions
	if ('page' == $_POST['post_type']) {
		if (!current_user_can('edit_page', $post_id)) {
			return $post_id;
		}
	} elseif (!current_user_can('edit_post', $post_id)) {
		return $post_id;
	}
	
	foreach ($meta_box['fields'] as $field) {
		$old = get_post_meta($post_id, $field['id'], true);
		$new = $_POST[$field['id']];
		
		if ($new && $new != $old) {
			update_post_meta($post_id, $field['id'], $new);
		} elseif ('' == $new && $old) {
			delete_post_meta($post_id, $field['id'], $old);
		}
	}
}
/* youtube metabox */
$prefix = 'dbtr_';

$meta_box2 = array(
	'id' => 'my-meta-box2',
	'title' => 'Youtube',
	'page' => 'attorneys',
	'context' => 'normal',
	'priority' => 'high',
	'fields' => array(
		array(
			'name' => 'Add Youtube',
			'desc' => 'Add iframe of youtube or other video service',
			'id' => $prefix . 'youtube',
			'type' => 'textarea',
			'std' => ''
		)
	)
);

add_action('admin_menu', 'mytheme_add_box2');

// Add meta box
function mytheme_add_box2() {
	global $meta_box2;
	
	add_meta_box($meta_box2['id'], $meta_box2['title'], 'mytheme_show_box2', $meta_box2['page'], $meta_box2['context'], $meta_box2['priority']);
}

// Callback function to show fields in meta box
function mytheme_show_box2() {
	global $meta_box2, $post;
	
	// Use nonce for verification
	echo '<input type="hidden" name="mytheme_meta_box2_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';
	
	echo '<table class="form-table">';

	foreach ($meta_box2['fields'] as $field2) {
		// get current post meta data
		$meta = get_post_meta($post->ID, $field2['id'], true);
		
		echo '<tr>',
				'<th style="width:20%"><label for="', $field2['id'], '">', $field2['name'], '</label></th>',
				'<td>';
		switch ($field2['type']) {
			case 'text':
				echo '<input type="text" name="', $field2['id'], '" id="', $field2['id'], '" value="', $meta ? $meta : $field2['std'], '" size="30" style="width:97%" />',
					'<br />', $field2['desc'];
				break;
			case 'textarea':
				echo '<textarea name="', $field2['id'], '" id="', $field2['id'], '" cols="60" rows="4" style="width:97%">', $meta ? $meta : $field2['std'], '</textarea>',
					'<br />', $field2['desc'];
				break;
			case 'select':
				echo '<select name="', $field2['id'], '" id="', $field2['id'], '">';
				foreach ($field2['options'] as $option) {
					echo '<option', $meta == $option ? ' selected="selected"' : '', '>', $option, '</option>';
				}
				echo '</select>';
				break;
			case 'radio':
				foreach ($field2['options'] as $option) {
					echo '<input type="radio" name="', $field2['id'], '" value="', $option['value'], '"', $meta == $option['value'] ? ' checked="checked"' : '', ' />', $option['name'];
				}
				break;
			case 'checkbox':
				echo '<input type="checkbox" name="', $field2['id'], '" id="', $field2['id'], '"', $meta ? ' checked="checked"' : '', ' />';
				break;
		}
		echo 	'<td>',
			'</tr>';
	}
	
	echo '</table>';
}

add_action('save_post', 'mytheme_save_data2');

// Save data from meta box
function mytheme_save_data2($post_id) {
	global $meta_box2;
	
	// verify nonce
	if (!wp_verify_nonce($_POST['mytheme_meta_box2_nonce'], basename(__FILE__))) {
		return $post_id;
	}

	// check autosave
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		return $post_id;
	}

	// check permissions
	if ('page' == $_POST['post_type']) {
		if (!current_user_can('edit_page', $post_id)) {
			return $post_id;
		}
	} elseif (!current_user_can('edit_post', $post_id)) {
		return $post_id;
	}
	
	foreach ($meta_box2['fields'] as $field) {
		$old = get_post_meta($post_id, $field['id'], true);
		$new = $_POST[$field['id']];
		
		if ($new && $new != $old) {
			update_post_meta($post_id, $field['id'], $new);
		} elseif ('' == $new && $old) {
			delete_post_meta($post_id, $field['id'], $old);
		}
	}
}
/* books select attorney */





$prefix = 'dbtr_';

$meta_box3 = array(
	'id' => 'my-meta-box3',
	'title' => 'Attorney',
	'page' => 'books',
	'context' => 'normal',
	'priority' => 'high',
	'fields' => array(
		array(
			'name' => 'Attorney',
			'desc' => 'Which attorney this book belongs to',
			'id' => $prefix . 'belongs',
			'type' => 'attorney',
			'std' => ''
		)
	)
);

add_action('admin_menu', 'mytheme_add_box3');

// Add meta box
function mytheme_add_box3() {
	global $meta_box3;
	
	add_meta_box($meta_box3['id'], $meta_box3['title'], 'mytheme_show_box3', $meta_box3['page'], $meta_box3['context'], $meta_box3['priority']);
}

// Callback function to show fields in meta box
function mytheme_show_box3() {
	global $meta_box3, $post;
	
	// Use nonce for verification
	echo '<input type="hidden" name="mytheme_meta_box3_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';
	
	echo '<table class="form-table">';

	foreach ($meta_box3['fields'] as $field2) {
		// get current post meta data
		$meta = get_post_meta($post->ID, $field2['id'], true);
		
		echo '<tr>',
				'<th style="width:20%"><label for="', $field2['id'], '">', $field2['name'], '</label></th>',
				'<td>';
		switch ($field2['type']) {
			case 'text':
				echo '<input type="text" name="', $field2['id'], '" id="', $field2['id'], '" value="', $meta ? $meta : $field2['std'], '" size="30" style="width:97%" />',
					'<br />', $field2['desc'];
				break;
			case 'textarea':
				echo '<textarea name="', $field2['id'], '" id="', $field2['id'], '" cols="60" rows="4" style="width:97%">', $meta ? $meta : $field2['std'], '</textarea>',
					'<br />', $field2['desc'];
				break;
			case 'select':
				echo '<select name="', $field2['id'], '" id="', $field2['id'], '">';
				foreach ($field2['options'] as $option) {
					echo '<option', $meta == $option ? ' selected="selected"' : '', '>', $option, '</option>';
				}
				echo '</select>';
				break;
			case 'attorney':
			echo '<select name="'.$field2['id'].'" id="'. $field2['id'].'">';
			global $post;
			$hver = new WP_Query("post_type=attorneys");
			while ( $hver->have_posts() ) : $hver->the_post();
			echo '<option';if($meta==get_the_ID()){ echo 'selected="selected"';} echo '>'; the_title();echo '</option>';
			endwhile;
			echo '</select>';
            break;			
			case 'radio':
				foreach ($field2['options'] as $option) {
					echo '<input type="radio" name="', $field2['id'], '" value="', $option['value'], '"', $meta == $option['value'] ? ' checked="checked"' : '', ' />', $option['name'];
				}
				break;
			case 'checkbox':
				echo '<input type="checkbox" name="', $field2['id'], '" id="', $field2['id'], '"', $meta ? ' checked="checked"' : '', ' />';
				break;
		}
		echo 	'<td>',
			'</tr>';
	}
	
	echo '</table>';
}

add_action('save_post', 'mytheme_save_data3');

// Save data from meta box
function mytheme_save_data3($post_id) {
	global $meta_box3;
	
	// verify nonce
	if (!wp_verify_nonce($_POST['mytheme_meta_box3_nonce'], basename(__FILE__))) {
		return $post_id;
	}

	// check autosave
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		return $post_id;
	}

	// check permissions
	if ('page' == $_POST['post_type']) {
		if (!current_user_can('edit_page', $post_id)) {
			return $post_id;
		}
	} elseif (!current_user_can('edit_post', $post_id)) {
		return $post_id;
	}
	
	foreach ($meta_box3['fields'] as $field) {
		$old = get_post_meta($post_id, $field['id'], true);
		$new = $_POST[$field['id']];
		
		if ($new && $new != $old) {
			update_post_meta($post_id, $field['id'], $new);
		} elseif ('' == $new && $old) {
			delete_post_meta($post_id, $field['id'], $old);
		}
	}
}
function shopping_pages($name)
{
global $wpdb;
// get page id using custom query
$page_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE ( post_name = '".$name."' or post_title = '".$name."' ) and post_status = 'publish' and post_type='page' ");
return $page_id;
}
//widgets
	class attorneyads_widget extends WP_Widget {

	// Constructor //

		function attorneyads_widget() {
			$widget_ops = array( 'classname' => 'attorneyads_widget', 'description' => 'Footer widget for displaying contact info' ); // Widget Settings
			$control_ops = array( 'id_base' => 'attorneyads_widget' ); // Widget Control Settings
			$this->WP_Widget( 'attorneyads_widget', 'Footer Contact Widget', $widget_ops, $control_ops ); // Create the widget
		}

	// Extract Args //

		function widget($args, $instance) {
			extract( $args );
			$title 		= apply_filters('widget_title', $instance['title']); // the widget title
			$title2 		= apply_filters('widget_title', $instance['title2']);
			$title3 		= apply_filters('widget_title', $instance['title3']); 
			$title4 		= apply_filters('widget_title', $instance['title4']); 
			$title5 		= apply_filters('widget_title', $instance['title5']); 
			$title6 		= apply_filters('widget_title', $instance['title6']); 


	// Widget output //

	
echo '<div class="fwidget fw2">';
            	echo '<p><strong>'.$title.'</strong>'.$title2.'<br />'.$title5.'<br />'.$title6.'</p>';
                echo '<div class="cont">';
                	echo '<span class="tele">'.$title3.'</span>';
                    echo '<span class="tele tele2">'.$title4.'</span>';
                echo '</div></div>';
	
	
		}

	// Update Settings //

		function update($new_instance, $old_instance) {
			$instance['title'] = strip_tags($new_instance['title']);
			$instance['title2'] = strip_tags($new_instance['title2']);
			$instance['title3'] = strip_tags($new_instance['title3']);
			$instance['title4'] = strip_tags($new_instance['title4']);
			$instance['title5'] = strip_tags($new_instance['title5']);
			$instance['title6'] = strip_tags($new_instance['title6']);
			return $instance;
		}

	// Widget Control Panel //

		function form($instance) {

		$defaults = array( 'title' => '');
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>'" type="text" value="<?php echo $instance['title']; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('title2'); ?>">Address:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('title2'); ?>" name="<?php echo $this->get_field_name('title2'); ?>'" type="text" value="<?php echo $instance['title2']; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('title5'); ?>">Address 2:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('title5'); ?>" name="<?php echo $this->get_field_name('title5'); ?>'" type="text" value="<?php echo $instance['title5']; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('title6'); ?>">Address 3:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('title6'); ?>" name="<?php echo $this->get_field_name('title6'); ?>'" type="text" value="<?php echo $instance['title6']; ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('title3'); ?>">Phone number:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('title3'); ?>" name="<?php echo $this->get_field_name('title3'); ?>'" type="text" value="<?php echo $instance['title3']; ?>" />
		</p>
						<p>
			<label for="<?php echo $this->get_field_id('title4'); ?>">Fax number:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('title4'); ?>" name="<?php echo $this->get_field_name('title4'); ?>'" type="text" value="<?php echo $instance['title4']; ?>" />
		</p>
        <?php }

}
//widgets
	class attorneyaop_widget extends WP_Widget {

	// Constructor //

		function attorneyaop_widget() {
			$widget_ops = array( 'classname' => 'attorneyaop_widget', 'description' => 'Sidebar widget for displaying  Areas of Practice' ); // Widget Settings
			$control_ops = array( 'id_base' => 'attorneyaop_widget' ); // Widget Control Settings
			$this->WP_Widget( 'attorneyaop_widget', 'Areas Of Practice', $widget_ops, $control_ops ); // Create the widget
		}

	// Extract Args //

		function widget($args, $instance) {
			extract( $args );
			$title 		= apply_filters('widget_title', $instance['title']); // the widget title

	// Widget output //

	
echo '<h2>'.$title.'</h2>';
echo '<div class="sidelinks"><ul>';
$kver = new WP_Query("post_type=areasofpractice&showposts=5");
while($kver->have_posts()):$kver->the_post();
echo '<li><a href="'.get_the_content().'">'.get_the_title().'</a></li>';
endwhile;
echo '</ul></div>'; }

	// Update Settings //

		function update($new_instance, $old_instance) {
			$instance['title'] = strip_tags($new_instance['title']);
			return $instance;
		}

	// Widget Control Panel //

		function form($instance) {

		$defaults = array( 'title' => '');
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>'" type="text" value="<?php echo $instance['title']; ?>" />
		</p>
        <?php }

}

// End class attorneyaop_widget

add_action('widgets_init', create_function('', 'return register_widget("attorneyaop_widget");'));
// End class attorneyads_widget

add_action('widgets_init', create_function('', 'return register_widget("attorneyads_widget");'));
//widgets
	class attorneyrecposts_widget extends WP_Widget {

	// Constructor //

		function attorneyrecposts_widget() {
			$widget_ops = array( 'classname' => 'attorneyrecposts_widget', 'description' => 'Recent posts widget' ); // Widget Settings
			$control_ops = array( 'id_base' => 'attorneyrecposts_widget' ); // Widget Control Settings
			$this->WP_Widget( 'attorneyrecposts_widget', 'Recent Posts Widget', $widget_ops, $control_ops ); // Create the widget
		}

	// Extract Args //

		function widget($args, $instance) {
			extract( $args );
			$title 		= apply_filters('widget_title', $instance['title']); // the widget title

	// Widget output //

	
                        echo '<h2>'.$title.'</h2><div class="recentpost-rbar"><ul>';
                                $novi = new WP_Query("post_type=post&showposts=3");
								while($novi->have_posts()):$novi->the_post();
                                    echo '<li>';
									
                                    	echo '<div class="image">';the_post_thumbnail('sidebar-thumb');echo '</a></div>';
                                    	echo '<h6><a href="'.get_permalink().'">';$caa3 = get_the_title();echo substr($caa3, 0, 22);echo '</a></h6>';print_excerpt(80);
                                    echo '</li>';
									endwhile;
									echo '</ul></div>';
	
	
		}

	// Update Settings //

		function update($new_instance, $old_instance) {
			$instance['title'] = strip_tags($new_instance['title']);
			return $instance;
		}

	// Widget Control Panel //

		function form($instance) {

		$defaults = array( 'title' => '');
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>'" type="text" value="<?php echo $instance['title']; ?>" />
		</p>
        <?php }

}

// End class attorneyrecposts_widget
add_action('widgets_init', create_function('', 'return register_widget("attorneyrecposts_widget");'));

// add support for excerpts on pages, posts etc..
add_post_type_support( 'page', 'excerpt' );
function paginate_linkz( $args = '' ) {

	$defaults = array(

		'base' => '%_%', // http://example.com/all_posts.php%_% : %_% is replaced by format (below)

		'format' => '?page=%#%', // ?page=%#% : %#% is replaced by the page number

		'total' => 1,

		'current' => 0,

		'show_all' => false,

		'prev_next' => true,

		'prev_text' => __('Previous'),

		'next_text' => __('Next'),

		'end_size' => 1,

		'mid_size' => 2,

		'type' => 'plain',

		'add_args' => false, // array of query args to add

		'add_fragment' => ''

	);



	$args = wp_parse_args( $args, $defaults );

	extract($args, EXTR_SKIP);



	// Who knows what else people pass in $args

	$total = (int) $total;

	if ( $total < 2 )

		return;

	$current  = (int) $current;

	$end_size = 0  < (int) $end_size ? (int) $end_size : 1; // Out of bounds?  Make it the default.

	$mid_size = 0 <= (int) $mid_size ? (int) $mid_size : 2;

	$add_args = is_array($add_args) ? $add_args : false;

	$r = '';

	$page_links = array();

	$n = 0;

	$dots = false;



	if ( $prev_next && $current && 1 < $current ) :

		$link = str_replace('%_%', 2 == $current ? '' : $format, $base);

		$link = str_replace('%#%', $current - 1, $link);

		if ( $add_args )

			$link = add_query_arg( $add_args, $link );

		$link .= $add_fragment;

		$page_links[] = '<a class="prev page-numbers" href="' . esc_url( apply_filters( 'paginate_links', $link ) ) . '">' . $prev_text . '</a>';

	endif;

	for ( $n = 1; $n <= $total; $n++ ) :

		$n_display = number_format_i18n($n);

		if ( $n == $current ) :

			$page_links[] = "<span class='page-numbers current'>$n_display ::</span>";

			$dots = true;

		else :

			if ( $show_all || ( $n <= $end_size || ( $current && $n >= $current - $mid_size && $n <= $current + $mid_size ) || $n > $total - $end_size ) ) :

				$link = str_replace('%_%', 1 == $n ? '' : $format, $base);

				$link = str_replace('%#%', $n, $link);

				if ( $add_args )

					$link = add_query_arg( $add_args, $link );

				$link .= $add_fragment;

				$page_links[] = "<a class='page-numbers' href='" . esc_url( apply_filters( 'paginate_links', $link ) ) . "'>$n_display ::</a>";

				$dots = true;

			elseif ( $dots && !$show_all ) :

				$page_links[] = '<span class="page-numbers dots">' . __( '&hellip;' ) . '</span>';

				$dots = false;

			endif;

		endif;

	endfor;

	if ( $prev_next && $current && ( $current < $total || -1 == $total ) ) :

		$link = str_replace('%_%', $format, $base);

		$link = str_replace('%#%', $current + 1, $link);

		if ( $add_args )

			$link = add_query_arg( $add_args, $link );

		$link .= $add_fragment;

		$page_links[] = '<a class="next page-numbers" href="' . esc_url( apply_filters( 'paginate_links', $link ) ) . '">' . $next_text . '</a>';

	endif;

	switch ( $type ) :

		case 'array' :

			return $page_links;

			break;

		case 'list' :

			$r .= "<ul class='page-numbers'>\n\t<li>";

			$r .= join("</li>\n\t<li>", $page_links);

			$r .= "</li>\n</ul>\n";

			break;

		default :

			$r = join("\n", $page_links);

			break;

	endswitch;

	return $r;

}
//clean shortcodes from p tags
// clean up shortcode

function parse_shortcode_content( $content ) {

   /* Parse nested shortcodes and add formatting. */


    /* Remove any instances of ''. */
    $content = str_replace( array( '<p>     </p>' ), '', $content );
	    $content = str_replace( array( '<p></p>' ), '', $content );
		    $content = str_replace( array( '<p>  </p>' ), '', $content );

    return $content;
}