<?php
/*
Template Name: Blog Template
*/
get_header(); ?>
 		<div id="banner-main-2">
        	<div class="indiv">
                <div class="slider">
                    <h2><?php the_title();?></h2>
                    <p><?php echo get_the_excerpt();?></p>
                    <div class="breadcrum">
                        <ul>
                            <li><a href="<?php echo home_url();?>">home</a></li>
                            <li class="last"><span><?php the_title();?></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
		<div id="container">
        	<div id="content">
                <div id="sec-content">                	
                    <div id="blog">
                    <?php
					global $post;
                                        global $wp_query;
                                       $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

					$args = array( 'posts_per_page' => 4,'paged' => $paged);

					$wp_query = new WP_Query( $args );

			 if ( $wp_query->have_posts() ) while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
                       


                    	<div class="tital">

                        	<h2><a href="<?php the_permalink() ?>" rel="bookmark" ><?php the_title(); ?></a></h2>

                            <div class="detail"><p>By : <?php the_author_posts_link() ?> | Category : <?php the_category(', '); ?> | <?php comments_popup_link('No Comments ', '1 Comment ', '% Comments '); ?></p></div>

                        </div>

                        <div class="section">                       

                        	<div class="date-box"><p><?php the_date('j\<\s\u\p\>S\<\/\s\u\p\> <\s\p\a\n\> M <\/\s\p\a\n\> Y'); ?></div>

                        	<div class="blog-image">
							<?php the_post_thumbnail('blog');?>
							</div>

                            <?php the_excerpt(); ?>

                        </div>

                        <?php endwhile; ?>
                <div id="content">
                <div id="pagination">
					<?php
    $total = $wp_query->max_num_pages;
    // Only paginate if we have more than one page
    if ( $total > 1 )  {
         // Get the current page
         if ( !$current_page = get_query_var('paged') )
              $current_page = 1;
         // sadd
        $permalinks = get_option('permalink_structure');
        $format = empty( $permalinks ) ? '&paged=%#%' : 'page/%#%/';
        echo paginate_linkz(array(
              'base' => get_pagenum_link(1) . '%_%',
              'format' => $format,
              'current' => $current_page,
              'total' => $total,
              'mid_size' => 2,
			      'prev_text'    => __('Previous'),
    'next_text'    => __('Next'),
              'type' => 'list'
        ));
    }
?>
				</div></div>
                    </div>
                    <div id="rightbar">
                    	<div class="search-box">
                        	<form id="searchform" action="<?php echo home_url();?>" method="get" >
                       <input onkeypress="" type="search" name="s" class="input-search" placeholder="Search..." />
                   </form>
                        </div>
                        <div class="sidelinks">
                             <?php get_sidebar( 'blog-widgets' );?>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>