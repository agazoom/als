<?php
/*
Template Name: Blog Template
*/
get_header(); ?>
 		<div id="banner-main-2">
        	<div class="indiv">
                <div class="slider">
                    <h2>Posts by: <?php the_post(); $byba = get_the_author();the_author();rewind_posts();?></h2>
                    <div class="breadcrum">
                        <ul>
                            <li><a href="<?php echo home_url();?>">home</a></li>
                            <li class="last"><span>Posts by: <?php echo $byba;?></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
		<div id="container">
        	<div id="content">
                <div id="sec-content">                	
                    <div id="blog">
                    <?php

			 if (have_posts() ) while ( have_posts() ) : the_post(); ?>
                       


                    	<div class="tital">

                        	<h2><a href="<?php the_permalink() ?>" rel="bookmark" ><?php the_title(); ?></a></h2>

                            <div class="detail"><p>By : <?php the_author_posts_link() ?> | Category : <?php the_category(', '); ?> | <?php comments_popup_link('No Comments ', '1 Comment ', '% Comments '); ?></p></div>

                        </div>

                        <div class="section">                       

                        	<div class="date-box"><p><?php the_date('j\<\s\u\p\>S\<\/\s\u\p\> <\s\p\a\n\> M <\/\s\p\a\n\> Y'); ?></div>

                        	<div class="blog-image">
							<?php the_post_thumbnail('blog');?>
							</div>

                            <?php the_excerpt(); ?>

                        </div>

                        <?php endwhile; ?>
                    </div>
                    <div id="rightbar">
                        <div class="sidelinks">
                             <?php get_sidebar( 'primary' );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>