<?php

add_action('init','of_options');

if (!function_exists('of_options'))
{
	function of_options()
	{
		//Access the WordPress Categories via an Array
		$of_categories = array();  
		$of_categories_obj = get_categories('hide_empty=0');
		foreach ($of_categories_obj as $of_cat) {
		    $of_categories[$of_cat->cat_ID] = $of_cat->cat_name;}
		$categories_tmp = array_unshift($of_categories, "Select a category:");    
	       
		//Access the WordPress Pages via an Array
		$of_pages = array();
		$of_pages_obj = get_pages('sort_column=post_parent,menu_order');    
		foreach ($of_pages_obj as $of_page) {
		    $of_pages[$of_page->ID] = $of_page->post_name; }
		$of_pages_tmp = array_unshift($of_pages, "Select a page:");       
	$of_pagess = array();
$of_pages_obj = get_pages('sort_column=post_parent,menu_order');    
foreach ($of_pages_obj as $of_page) {
    $of_pagess[$of_page->ID] = $of_page->post_name; }
$of_pages_tmp = array_unshift($of_pagess, "Select a page:");  
		//Testing 
		$of_options_select = array("one","two","three","four","five"); 
		$of_options_radio = array("one" => "One","two" => "Two","three" => "Three","four" => "Four","five" => "Five");
		
		//Sample Homepage blocks for the layout manager (sorter)
		$of_options_homepage_blocks = array
		( 
			"disabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
				"block_one"		=> "Block One",
				"block_two"		=> "Block Two",
				"block_three"	=> "Block Three",
			), 
			"enabled" => array (
				"placebo" => "placebo", //REQUIRED!
				"block_four"	=> "Block Four",
			),
		);


		//Stylesheets Reader
		$alt_stylesheet_path = LAYOUT_PATH;
		$alt_stylesheets = array();
		
		if ( is_dir($alt_stylesheet_path) ) 
		{
		    if ($alt_stylesheet_dir = opendir($alt_stylesheet_path) ) 
		    { 
		        while ( ($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false ) 
		        {
		            if(stristr($alt_stylesheet_file, ".css") !== false)
		            {
		                $alt_stylesheets[] = $alt_stylesheet_file;
		            }
		        }    
		    }
		}


		//Background Images Reader
		$bg_images_path = STYLESHEETPATH. '/images/bg/'; // change this to where you store your bg images
		$bg_images_url = get_template_directory_uri().'/images/bg/'; // change this to where you store your bg images
		$bg_images = array();
		
		if ( is_dir($bg_images_path) ) {
		    if ($bg_images_dir = opendir($bg_images_path) ) { 
		        while ( ($bg_images_file = readdir($bg_images_dir)) !== false ) {
		            if(stristr($bg_images_file, ".png") !== false || stristr($bg_images_file, ".jpg") !== false) {
		                $bg_images[] = $bg_images_url . $bg_images_file;
		            }
		        }    
		    }
		}
		

		/*-----------------------------------------------------------------------------------*/
		/* TO DO: Add options/functions that use these */
		/*-----------------------------------------------------------------------------------*/
		
		//More Options
		$uploads_arr = wp_upload_dir();
		$all_uploads_path = $uploads_arr['path'];
		$all_uploads = get_option('of_uploads');
		$other_entries = array("Select a number:","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19");
		$body_repeat = array("no-repeat","repeat-x","repeat-y","repeat");
		$body_pos = array("top left","top center","top right","center left","center center","center right","bottom left","bottom center","bottom right");
		
		// Image Alignment radio box
		$of_options_thumb_align = array("alignleft" => "Left","alignright" => "Right","aligncenter" => "Center"); 
		
		// Image Links to Options
		$of_options_image_link_to = array("image" => "The Image","post" => "The Post"); 


/*-----------------------------------------------------------------------------------*/
/* The Options Array */
/*-----------------------------------------------------------------------------------*/

// Set the Options Array
global $of_options;
$of_options = array();
					
$of_options[] = array( "name" => "General Settings",
                    "type" => "heading");
					
$url =  ADMIN_DIR . 'assets/images/';
$of_options[] = array( "name" => "Logo Upload",
					"desc" => "Recommended size is 153x56",
					"id" => "logo",
					"std" => "",
					"type" => "media");
					
$of_options[] = array( "name" => "Logo subline",
					"desc" => "",
					"id" => "logoline",
					"std" => "Subline for logo",
					"type" => "text"); 
					
$of_options[] = array( "name" => "Footer Logo Upload",
					"desc" => "Recommended size is 150x69",
					"id" => "logo2",
					"std" => "",
					"type" => "media");
$of_options[] = array( "name" => "Header Phone Number",
					"desc" => "",
					"id" => "phone",
					"std" => "(800) 123-4567",
					"type" => "text"); 
					
$of_options[] = array( "name" => "Header Phone text",
					"desc" => "",
					"id" => "phone2",
					"std" => "Call us today for a Free Legal Consultation",
					"type" => "text"); 
					
$of_options[] = array( "name" => "Twitter username",
					"desc" => "enter only username",
					"id" => "twitter",
					"std" => "username",
					"type" => "text"); 					
$of_options[] = array( "name" => "Facebook URL",
					"desc" => "Facebook link",
					"id" => "facebook",
					"std" => "http://facebook.com/yourname",
					"type" => "text"); 		

$of_options[] = array( "name" => "Flickr URL",
					"desc" => "Flickr link",
					"id" => "flickr",
					"std" => "http://flickr.com",
					"type" => "text"); 
$of_options[] = array( "name" => "Footer copyright text",
					"desc" => "",
					"id" => "copyright",
					"std" => "Copyright 2012. All Rights Reserved by Attorneys. Designed by sunil.",
					"type" => "text"); 	
$of_options[] = array( "name" => "Contact email address",
					"desc" => "Enter email address where emails from contact page should be sent",
					"id" => "kontakt",
					"std" => "example@mail.com",
					"type" => "text");				
$of_options[] = array( "name" => "Map Latitude",
					"desc" => "Latitude of your location",
					"id" => "glat",
					"std" => "",
					"type" => "text");	
$of_options[] = array( "name" => "Map Longitude",
					"desc" => "Longitude of your location",
					"id" => "glon",
					"std" => "",
					"type" => "text");															
$of_options[] = array( "name" => "Contact address",
					"desc" => "Used on contact page",
					"id" => "contactaddress",
					"std" => "Some address here 17th floor New York",
					"type" => "text"); 	
					
$of_options[] = array( "name" => "Contact phone number",
					"desc" => "Please use br tag to make new line, this is used on contact page.",
					"id" => "contactphone",
					"std" => "Tel : +31 252 62 61 20<br>Us Tel : +1 858 - 926 5462",
					"type" => "text"); 
$of_options[] = array( "name" => "Contact fax number",
					"desc" => "Please use br tag to make new line, this is used on contact page.",
					"id" => "contactfax",
					"std" => "Fax : +31 252 62 61 20",
					"type" => "text");			
$of_options[] = array( "name" => "Contact Page URL",
					"desc" => "After you make contact page enter URL of page here",
					"id" => "ctt",
					"std" => "http://yoursite.com/contact-us",
					"type" => "text");  					 					
$of_options[] = array( "name" => "Home Settings",
					"type" => "heading");
					
$of_options[] = array( "name" => "Tagline",
					"desc" => "Enter tagline which appears under slider on homepage",
					"id" => "tagline",
					"std" => "",
					"type" => "textarea");  
$of_options[] = array( "name" => "Tagline button text",
					"desc" => "Text on yellow button",
					"id" => "button",
					"std" => "Call To Action<span>Subtitle tab goes here for slogan</span>",
					"type" => "text");  	
$of_options[] = array( "name" => "Select Homepage Page(appears under tagline)",
					"desc" => "",
					"id" => "homepage_page",
					"std" => "Select page:",
					"type" => "select",
					"options" => $of_pagess);					
$of_options[] = array( "name" => "Tagline link",
					"desc" => "Link on tagline",
					"id" => "link",
					"std" => "http://yoursite.com",
					"type" => "text"); 
$of_options[] = array( "name" => "Hide Testimonials",
					"desc" => "If you don't want to display testimonials on the home page please check this",
					"id" => "testitrue",
					"std" => 0,
					"type" => "checkbox"); 	
$of_options[] = array( "name" => "Hide Awards",
					"desc" => "If you don't want to display Awards & Accomplishments on the home page please check this",
					"id" => "awardstrue",
					"std" => 0,
					"type" => "checkbox"); 	  										
//slider home
$of_options[] = array( "name" => "Homepage Slider",
					"type" => "heading");		
$of_options[] = array( "name" => "Info",
					"desc" => "",
					"id" => "intros",
					"std" => "<h3 style=\"margin: 0 0 10px;\">This is area for adding slides on homepage slider</h3>
					Please use field Description to add text for slide, you can use HTML there. You can also use HTML in Title field to change color or similar.",
					"icon" => true,
					"type" => "info");					
$of_options[] = array( "name" => "Slides",
	"desc" => "",
	"id" => "example_slider",
	"std" => "",
	"type" => "slider");		
//awards
$of_options[] = array( "name" => "Awards",
					"type" => "heading");		
$of_options[] = array( "name" => "Info",
					"desc" => "",
					"id" => "intros2",
					"std" => "<h3 style=\"margin: 0 0 10px;\">This area is for uploading Awards images (they appear on homepage)</h3>
					Please only upload images, you can easily reorder them by drag&drop feature.",
					"icon" => true,
					"type" => "info");					
$of_options[] = array( "name" => "Awards",
	"desc" => "",
	"id" => "example_slider2",
	"std" => "",
	"type" => "slider");	
//options for color picker etc
$of_options[] = array( "name" => "Styling Settings",
					"type" => "heading");
$of_options[] = array (  "name" => "Style Sheet",
	"desc" => "Please select the Style Sheet you would like to use",
	"id" => "style_sheet",
	"type" => "select",
	"options" => array("default", "blue"), 
	"std" => "default");					
$of_options[] = array( "name" => "Use default colors",
					"desc" => "If you like to use custome colors/background images instead of the pre-made styles please uncheck this",
					"id" => "ftrue",
					"std" => 1,
					"type" => "checkbox"); 						
$of_options[] = array( "name" =>  "Link & Menu Buttons Color",
					"desc" => "Pick a background color for links and main menu buttons",
					"id" => "linkcolor",
					"std" => "#",
					"type" => "color");							
$of_options[] = array( "name" =>  "Header Background Color",
					"desc" => "Pick a background color for the header (default is #fff)",
					"id" => "hbg",
					"std" => "#",
					"type" => "color");
$of_options[] = array( "name" =>  "Slider & Menu Background Color",
					"desc" => "Pick a background color for the top part of the slider background",
					"id" => "sbg",
					"std" => "#",
					"type" => "color");						
$of_options[] = array( "name" => "Slider & Menu Background Image",
					"desc" => "Pick a background image for the top part of the slider background",
					"id" => "background_s",
					"std" => "",
					"type" => "media");	
$of_options[] = array( "name" =>  "Slider Second Background Color",
					"desc" => "Pick a background color for the bottom part of the slider background",
					"id" => "sbg2",
					"std" => "#",
					"type" => "color");						
$of_options[] = array( "name" => "Slider Second Background Image",
					"desc" => "ick a background imge for the bottom part of the slider background",
					"id" => "background_s2",
					"std" => "",
					"type" => "media");	
$of_options[] = array( "name" =>  "Container Background Color",
					"desc" => "Pick a background color for the middle of the page",
					"id" => "cbg",
					"std" => "#",
					"type" => "color");						
$of_options[] = array( "name" => "Container Background Image",
					"desc" => "Pick a background image for the middle of the page",
					"id" => "background_c",
					"std" => "",
					"type" => "media");										
	
$of_options[] = array( "name" =>  "Footer Background Color",
					"desc" => "Pick a background color for the footer",
					"id" => "fbg",
					"std" => "#",
					"type" => "color");	
$of_options[] = array( "name" => "Footer Background Image",
					"desc" => "Pick a background image for the footer",
					"id" => "background_f",
					"std" => "",
					"type" => "media");	
				
// Backup Options
$of_options[] = array( "name" => "Backup Options",
					"type" => "heading");
					
$of_options[] = array( "name" => "Backup and Restore Options",
                    "id" => "of_backup",
                    "std" => "",
                    "type" => "backup",
					"desc" => 'You can use the two buttons below to backup your current options, and then restore it back at a later time. This is useful if you want to experiment on the options but would like to keep the old settings in case you need it back.',
					);
					
$of_options[] = array( "name" => "Transfer Theme Options Data",
                    "id" => "of_transfer",
                    "std" => "",
                    "type" => "transfer",
					"desc" => 'You can tranfer the saved options data between different installs by copying the text inside the text box. To import data from another install, replace the data in the text box with the one from another install and click "Import Options".
						',
					);
					
	}
}
?>
