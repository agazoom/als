<div id="comments">
<?php if ( have_comments() ) : ?>
			
			<h2 id="comments-title"><?php
			printf( _n( 'Comments (1)', ' Comments (%1$s)', get_comments_number(), 'attorneys' ),number_format_i18n( get_comments_number() ), '<em>' . get_the_title() . '</em>' );
			?></h2>
            
           


			<ul>
				<?php
					wp_list_comments( array( 'callback' => 'attorneys_comment' ) );
				?>
			</ul>

<?php else :
	if ( ! comments_open() ) :
?>
	<p class="nocomments"><?php _e( 'Comments are closed.', 'attorneys' ); ?></p>
<?php endif; // end ! comments_open() ?>

<?php endif; // end have_comments() ?>
</div><!-- #comments -->

<?php 
$comment_args = array( 'fields' => apply_filters( 'comment_form_default_fields', array(
    'author' => '<p class="comment-form-author">' .
                ( $req ? '<span class="required">*</span>' : '' ) .
                '<input id="author" name="author" type="text" value="Name" onblur="if(this.value==\'\')this.value=\'Name\';" onfocus="if(this.value==\'Name\')this.value=\'\';" value="Name" size="30" tabindex="1"' . $aria_req . ' />' .
				'<label for="author">* required field</label> ' .
                '</p><!-- #form-section-author .form-section -->',
    'email'  => '<p class="comment-form-email">' .
                ( $req ? '<span class="required">*</span>' : '' ) .
                '<input id="email" name="email" type="text" value="Email" onblur="if(this.value==\'\')this.value=\'Email\';" onfocus="if(this.value==\'Email\')this.value=\'\';" size="30" tabindex="2"' . $aria_req . ' />' .
				'<label for="email">* required field</label> ' .
                '</p><!-- #form-section-email .form-section -->',
				'<!-- #<span class="hiddenSpellError" pre="">form-section-url</span> .form-section -->' ) ),
    'comment_field' => '<p class="comment-form-comment">' .
                '<textarea id="comment" name="comment" cols="45" rows="8" tabindex="4" aria-required="true" value="Message" onblur="if(this.value==\'\')this.value=\'Message\';" onfocus="if(this.value==\'Message\')this.value=\'\';">Message</textarea>' .
                '</p><!-- #form-section-comment .form-section -->',
    'must_log_in' => '
<p class="must-log-in">' .  sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>

',
    'logged_in_as' => '
<p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%s">%s</a>. <a title="Log out of this account" href="%s">Log out?</a></p>

' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ),
    'comment_notes_before' => '',
    'comment_notes_after' => '',
    'id_form' => 'commentform',
    'id_submit' => 'submit',
    'title_reply' => __( 'Leave a Reply' ),
    'title_reply_to' => __( 'Leave a Reply to %s' ),
    'cancel_reply_link' => __( 'Cancel reply' ),
    'label_submit' => __( 'Post Comment' ),
);
comment_form($comment_args);?>