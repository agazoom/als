<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
		<div id="banner-main-2">
        	<div class="indiv">
                <div class="slider">
                    <h2>Blog</h2>
                    <p></p>
                    <div class="breadcrum">
                       <ul>
                             <li><a href="<?php bloginfo('url');?>">Home</a> Blog page</li>
                           
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
		<div id="container">
        	<div id="content">
            	
                <div id="sec-content">                	
						
					
                    <div id="blog">
                         	<?php while ( have_posts() ) : the_post(); ?>


    <?php
$category = get_the_category($post_ID);

?>
                    	<div class="tital">
                        	<h2><a href="<?php the_permalink() ?>" rel="bookmark" ><?php the_title(); ?></a></h2>
                            <div class="detail"><p>By : <?php the_author_posts_link() ?> | Category : <?php the_category(', '); ?> | <?php comments_popup_link('No Comments ', '1 Comment ', '% Comments '); ?></p></div>
                        </div>
                        <div class="section">                       
                        	<div class="date-box"><p><?php the_date('j\<\s\u\p\>S\<\/\s\u\p\> <\s\p\a\n\> M <\/\s\p\a\n\> Y'); ?></div>
                        	<div class="blog-image"><?php the_post_thumbnail('blog');?></div>
                            <?php the_content(); ?>
                        </div>
                        <?php endwhile; // end of the loop. ?>                   
                        
                                              
                        <?php comments_template(); ?>
                    </div>
                    
                    
                    <div id="rightbar">
                    	
                        <div class="sidelinks">
                             <?php get_sidebar( 'blog-widgets' );?>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
        
   
<?php get_footer(); ?>