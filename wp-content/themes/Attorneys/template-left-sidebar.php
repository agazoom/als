<?php
/*
Template Name: Left Sidebar Template
*/
get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); $knot = get_the_content();$knot = apply_filters('the_content', $knot);?>
		<div id="banner-main-2">
        	<div class="indiv">
                <div class="slider">
                    <h2><?php the_title()?></h2>
                    <p><?php echo get_the_excerpt();?></p>
                    <div class="breadcrum">
                        <ul>
                            <li><?php the_breadcrumb()?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div id="container">
            <div id="content">
            <div id="sec-content">
			                    <div id="rightbar" style="float:left">
                    	
                        <div class="sidelinks">
                            <?php get_sidebar('primary');?>
                        </div>
                    </div>
        	<div class="content" style="float:right">
			<div id="mid-content" style="width:640px">
                  <?php echo $knot; ?>
				  </div>
                </div>
            </div>
        </div>
        </div>
<?php endwhile;get_footer(); ?>