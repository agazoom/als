<?php
/*
Template Name: Attorney  Template
*/
get_header();
global $wp_query;
$big = 999999999; // need an unlikely integer
?>
<div id="banner-main-2">
        	<div class="indiv">
                <div class="slider">
                    <h2>Attorneys</h2>
                    <p><?php echo get_the_excerpt();?></p>
					<div class="breadcrum">
                        <ul>
                            <li><a href="<?php echo home_url();?>">home</a></li>
                            <li class="last"><span>Attorneys</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
<div id="container">
        	<div id="content">
            	
                <div id="profile-con">
                	<div class="pro-list">
                        		<?php
								$count =0;
								$class = '';
								$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                $attorn = new WP_Query("post_type=attorneys&showposts=6&paged=".$paged);
								while($attorn->have_posts()):$attorn->the_post();
								$count++;
                                ?>
                        <?php if($count%3==0){ $class = 'class = "block pro-list-last"'; } else { $class = 'class = "block"'; }?>                                           
                        
                        <div <?php echo $class; ?>>
                            <div class="image"><a href="<?php the_permalink();?>"><?php the_post_thumbnail('attorney');?></a></div>
                            <div class="con-meet">
                                <p><strong><a href="<?php the_permalink();?>"><?php the_title();?></a> </strong><?php $caa2 = get_the_excerpt();
echo substr($caa2, 0, 161);?>...<a href="<?php the_permalink();?>">more</a></p>
                            </div>
                        </div>
                        <?php endwhile; ?>
                        
                    </div>
                </div>
                <div id="content">
                <div id="pagination" style="width: 980px;">
					<?php
    $total = $attorn->max_num_pages;
    // Only paginate if we have more than one page
    if ( $total > 1 )  {
         // Get the current page
         if ( !$current_page = get_query_var('paged') )
              $current_page = 1;
         // Structure of �format� depends on whether we�re using pretty permalinks
        $permalinks = get_option('permalink_structure');
        $format = empty( $permalinks ) ? '&paged=%#%' : 'page/%#%/';
        echo paginate_linkz(array(
              'base' => get_pagenum_link(1) . '%_%',
              'format' => $format,
              'current' => $current_page,
              'total' => $total,
              'mid_size' => 2,
              'type' => 'list'
        ));
    }
?>
				</div></div>
                
            </div>
        </div>
        

       
<?php get_footer(); ?>