<?php
get_header();
?>

<div id="banner-main-2">
        	<div class="indiv">
                <div class="slider">
                    <h2><?php the_title();?></h2>
                    <p><?php echo $data['subhead'];?></p>
					<div class="breadcrum">
                        <ul>
                            <li><a href="<?php echo home_url();?>">home</a></li>
                            <li class="last"><span><?php the_title();?></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
<div id="container">
        	<div id="content">
            	
                <div id="profile-con">
				<?php 
				while(have_posts()):the_post();
				$keep = get_the_title();
                ?>                	
                    <div class="pro-detail">
                    	<div class="image"><?php the_post_thumbnail('attorney2');?></div>
                        <h4><?php the_title();?>, </h4>
                        <p><?php echo get_the_content();
						$awe = get_post_custom();?></p>
						<div class="clear"></div>
                        <div class="books-list">
					<?php 
								$hasposts = get_posts('post_type=books');
								if( !empty ( $hasposts ) ) {
								echo "<h3> Books by ",the_title(),"</h3>";
};
 ?>    
                            <ul>					<?php endwhile;?>
                            <?php 
							$argz = array(
	'post_type' => 'books',
	'meta_query' => array(
		array(
			'value' => $keep,
			'compare' => 'LIKE'
		)
	)
 );
							$query = new WP_Query($argz);
							while($query->have_posts()):$query->the_post();
							?>
                            	<li>
                                	<div class="b-img"><?php the_post_thumbnail('book');?></div>
                                    <p><?php the_title();?></p>
                                </li>
                                <?php endwhile;?>
                            </ul>
                        </div>
                    </div>
                	<div class="rightside">
                    	<div class="block">
                            <div class="image"><?php 
							echo $awe[dbtr_youtube][0];
							?> </div>
                            <div class="con-meet">
                                <p>You can contact me by clicking below button.</p>
                                <a href="<?php echo $data['ctt'];?>" class="contact-btn">Contact Attorneys</a>
                            </div>
                        </div>
                    </div>
                </div>              
            </div>
        </div>
    </div>
<?php get_footer(); ?>