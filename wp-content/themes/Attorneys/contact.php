<?php
/*
Template Name: Contact  Us
*/
get_header();
global $data; ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div id="banner-main-2">
        	<div class="indiv">
                <div class="slider">
                    <h2><?php the_title()?></h2>
                    <div class="breadcrum">
                        <ul>
                            <li><?php the_breadcrumb()?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <div id="container">
	   <div id="content">
    		<div id="mid-content" style="padding-top:30px">

                <?php the_content();?>
                
                <div class="con-info">
                    <div class="address-box">
                        <div class="address">
                            <p><?php echo $data['contactaddress'];?></p>
                            <p>&nbsp;</p><p>&nbsp;</p>
                            <div class="footnote">Entry and exit to <strong>Bay Subway Station</strong> is conveniently located
                            in the building.</div>
                        </div>
                        <div class="map" id="map"></div>
                    </div>
                    
                    <div class="con-details">
                        <div class="telephone">
                            <p><?php echo $data['contactphone'];?></p>
                        </div>
                        <div class="mail">
                            <p><?php echo $data['contactfax'];?></p>
                        </div>
                    </div>
                </div>


                <div class="contactus-form">
                    <h3>Send Us a Note</h3>
                    <?php echo do_shortcode('[formidable id=6]'); ?>
                </div>


            </div>
        </div>
<?php endwhile; ?>
<?php get_footer(); ?>
