<?php
/*
Template Name: Right Sidebar Template
*/
get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<div id="banner-main-2">
        	<div class="indiv">
                <div class="slider">
                    <h2><?php the_title()?></h2>
                    <div class="breadcrum">
                        <ul>
                            <li><?php the_breadcrumb()?></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div id="container">
            <div id="content">
            <div id="sec-content">
        	<div class="content" id="mid-content" style="width: 640px;">

                
                  <?php the_content(); ?>
                </div>
                    <div id="rightbar">
                        <div class="sidelinks">
                             <?php get_sidebar( 'primary' );?>
                        </div>
                    </div>
            </div>
        </div>
        </div>
<?php endwhile;get_footer(); ?>