<?php
function accordions_shortcode($atts, $content){
    ob_start(); ?>
    <div class="basic" style="float:left; margin-left: 2em;" id="list1b">
      <?php echo do_shortcode( $content ); ?>
    </div>
    <?php
    return ob_get_clean();
}

function accordion_shortcode($atts, $content){
    extract(shortcode_atts(array(
		'title' => null		
	), $atts));
	ob_start();
     ?><a><span></span><?php echo $atts[title]; ?></a><div class="acc_expant"><?php 
echo $content;?></div>
    <?php
    return ob_get_clean();
}

function toggle_shortcode($atts, $content){
    ob_start(); ?>
    <div class="toggle">
    <div class="basic1" style="float:left; margin-left: 2em;" id="list1b">
       <?php echo do_shortcode( $content ); ?>
    </div>
    </div> <?php
    return ob_get_clean();
}

function toggletext_shortcode($atts, $content){
    ob_start(); ?>
    <a><span></span> <?php  echo( $atts['title'] ); ?></a>
    <div class="acc_expant">
    <p><?php echo  trim($content) ; ?></p>
    </div>
    <?php return ob_get_clean();
 }

function tabs_shortcode($atts, $content){
     global $single_tab_array;
    $single_tab_array = array(); // clear the array
  
      ob_start();    do_shortcode($content); ?>
      <div id="tabs">

      <ul>
      <?php $count=0; foreach ($single_tab_array as $tab => $tab_attr_array) {$count++; ?>
          <li><a href="#tab-<?php echo $count?>"><?php echo $tab_attr_array['title'];?></a></li>
      <?php } ?>
      </ul>
      <?php $count=0; foreach ($single_tab_array as $tab => $tab_attr_array) {$count++; ?>
      <div id="tab-<?php echo $count?>">
      <?php echo $tab_attr_array['content'];?>
      </div>
       <?php } ?>
      </div>
      <?php return ob_get_clean();
}

function tab_shortcode($atts, $content){
extract(shortcode_atts(array(
	    'title'      => '',
    ), $atts));
    global $single_tab_array;
    $single_tab_array[] = array('title' => $title, 'content' => trim(do_shortcode($content)));
    return $single_tab_array;
}

function table_shortcode($atts, $content){
    global $single_row_array;
    global $row_count;
    $row_count=0;
    $single_row_array = array();   
    do_shortcode($content);
    ob_start();  ?>
       <table cellpadding="0" cellspacing="0" border="0" class="table-1">

      <?php    $row= $single_row_array[1]; ?>
           <thead>
               <tr>
                 <?php 
				 $brojis = 0;
				 foreach($row as $data){ 
				 $brojis++;
				 if($brojis==1){ $velicina = 5; $teksto = "border-top-left-radius: 5px;";}
				 if($brojis==2){ $velicina = 590;}
				 if($brojis==3){ $velicina = 150;}
				 if($brojis==4){ $velicina = 110; $teksto = "border-top-right-radius: 5px;";}?>
                   <th width="<?php echo $velicina;?>" style="<?php echo $teksto;?>"><p><?php  echo $data;?></p></th>
                 <?php } unset( $single_row_array[1]); ?>
               </tr>
           </thead>

            <tbody>
            <?php  			$brojismoj = 0;foreach ($single_row_array as $row){
			$brojismoj++;	

                ?>

            <tr <?php if($brojismoj==3){ echo 'style="background:#fde8dd;"';}?>>
            <?php   
			foreach($row as $data){		?>
            <td align="center"><p><?php echo $data; ?></p></td>
            <?php }?>
            </tr>
           <?php } ?>
            </tbody>

       </table>
    <?php


    return ob_get_clean();
}

function table_row_shortcode($atts, $content){
      global $single_row_array; global $data_count; global $row_count;
      $data_count=0; $row_count++;
      do_shortcode($content);
      $data_count=0;
}

function table_row_data_shortcode($atts, $content){
       global $single_row_array; global $data_count; global $row_count;
       $single_row_array[$row_count][$data_count]=trim(do_shortcode($content));
       $data_count++;
}

add_shortcode("accordion",accordion_shortcode);
add_shortcode("accordions",accordions_shortcode);
add_shortcode("toggle",toggle_shortcode);
add_shortcode("text",toggletext_shortcode);

/* Shortcode: tabs
 * Usage:   [tabs]
 * 		[tab title="title 1"]Your content goes here...[/tab]
 * 		[tab title="title 2"]Your content goes here...[/tab]
 * 	    [/tabs]
 */
add_shortcode("tabs",tabs_shortcode);
add_shortcode("tab",tab_shortcode);

/* Shortcode: tables
 * Usage
 *        [table]
 *           [row]
 *             [data] col 1 [/data]
 *             [data] col 2 [/data]
 *             [data] col 3 [/data]
 *           [/tr]
 *           [row]
 *             [data] data 1 [/data]
 *             [data] data 2 [/data]
 *             [data] data 3 [/data]
 *           [/row]
 *        [/table]
 *
 *
 *
 */

add_shortcode("table",table_shortcode);
add_shortcode("row",table_row_shortcode);
add_shortcode("data",table_row_data_shortcode);
function gridfull_shortcode($atts, $content){
    extract(shortcode_atts(array(
		'title' => null		
	), $atts));
	ob_start();
     ?>
     <div class="gridy gridfull"><strong><?php echo $atts[title]; ?></strong>
     <?php echo $content ?>
     </div>
    <?php
    return ob_get_clean();
}
add_shortcode("gridfull",gridfull_shortcode);
function gridhalf_shortcode($atts, $content){
    extract(shortcode_atts(array(
		'title' => null		
	), $atts));
	ob_start();
     ?>
     <div class="gridy gridhalf"><strong><?php echo $atts[title]; ?></strong>
     <?php echo $content ?>
     </div>
    <?php
    return ob_get_clean();
}
add_shortcode("gridhalf",gridhalf_shortcode);
function gridthird_shortcode($atts, $content){
    extract(shortcode_atts(array(
		'title' => null		
	), $atts));
	ob_start();
     ?>
     <div class="gridy gridthird"><strong><?php echo $atts[title]; ?></strong>
     <?php echo $content ?>
     </div>
    <?php
    return ob_get_clean();
}
add_shortcode("gridthird",gridthird_shortcode);
function gridfourth_shortcode($atts, $content){
    extract(shortcode_atts(array(
		'title' => null		
	), $atts));
	ob_start();
     ?>
     <div class="gridy gridfourth"><strong><?php echo $atts[title]; ?></strong>
     <?php echo $content ?>
     </div>
    <?php
    return ob_get_clean();
}
add_shortcode("gridfourth",gridfourth_shortcode);
?>
