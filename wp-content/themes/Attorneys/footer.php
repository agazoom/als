	    <?php
		global $data;?>
		<div id="footer">
    	<div class="footer">
<div class="block-1">
            	<div class="logo"><img src="<?php echo $data['logo2'];?>" alt="logo" /></div>
                <p><?php echo $data['copyright'];?></p>
                <div class="social">
                	<a href="http://twitter.com/<?php echo $data['twitter'];?>"><img src="<?php echo get_template_directory_uri();?>/images/twitter-icon.png" border="0" alt="" /></a>
                    <a href="<?php echo $data['facebook'];?>"><img src="<?php echo get_template_directory_uri();?>/images/facebook-icon.png" border="0" alt="" /></a>
                    <a href="<?php bloginfo('rss_url');?>"><img src="<?php echo get_template_directory_uri();?>/images/rss-icon.png" border="0" alt="" /></a>
                </div>
            </div>			
			<?php get_sidebar( 'footer-widgets' );?>
        </div>
    </div>
<?php
wp_footer();
?>
</body>
</html>
