<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged,$wpdb,$data;
	wp_title( '|', true, 'right' );
	bloginfo( 'name' );
	// Add the blog name.
	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'attorneys' ), max( $paged, $page ) );
	?></title>
<?php wp_enqueue_script("jquery"); ?>
<link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="stylesheet" href="<?php if ($data['style_sheet'] == "blue") {print bloginfo('template_directory')."/style-blue.css";} else {print bloginfo('stylesheet_url'); }  ?>" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php if ($data['style_sheet'] == "blue") {print bloginfo('template_directory')."/ddsmoothmenu-blue.css";} else {print bloginfo('template_directory')."/ddsmoothmenu.css";}  ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style/slider.css" />
<?php
	if ( is_singular() && get_option( 'thread_comments' ) )

		wp_enqueue_script( 'comment-reply' );
	wp_head();
?>
<link href="<?php echo get_template_directory_uri(); ?>/fonts/fonts.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,400italic,600italic,700italic' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/ddsmoothmenu.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
  		$("#slider").easySlider({
			auto: true, 
			continuous: true
		});
		//footer css
		$(".fwidget").each(function (index, domEle) {
		var getit = index+2;
		$(domEle).addClass("block-"+getit);
});
  var body = $( 'body' );
});
ddsmoothmenu.init({

	mainmenuid: "smoothmenu1", //menu DIV id

	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"

	classname: 'ddsmoothmenu', //class added to menu's outer DIV

	//customtheme: ["#1c5a80", "#18374a"],

	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]

});
      function initialize() {
	var myLatlng = new google.maps.LatLng(<?php if  ($data['glat']) {echo $data['glat'];} else {echo "1";}?>,<?php if  ($data['glon']) {echo $data['glon'];} else {echo "1";}?>);
    var mapOptions = {
    zoom: 14,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map = new google.maps.Map(document.getElementById('map'), mapOptions);

  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title:"<?php echo $data['contactaddress'] ?>"
  });
      } 
</script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/easySlider1.7.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style/demo.css" />
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/chili-1.7.pack.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.dimensions.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.accordion.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
		$('#tabs div').hide();
		$('#tabs div:first').show();
		$('#tabs ul li:first').addClass('active');
		$('#tabs ul li a').click(function(){
		$('#tabs ul li').removeClass('active');
		$(this).parent().addClass('active');
		var currentTab = $(this).attr('href');
		$('#tabs div').hide();
		$(currentTab).show();
		return false;
		});
		  var body = $( 'body' );
	});
	jQuery().ready(function(){
		// simple accordion
		jQuery('#list1a').accordion();
		jQuery('#list1b').accordion({
			autoheight: false
		});
	});

        function submitx(){

            if(document.getElementById("name").value=="") {alert("Please Enter Name"); return;}
            if(document.getElementById("email").value=="") {alert("Please Enter Email"); return;}
            if(document.getElementById("message").value=="") {alert("Please Enter Message"); return;}
            this.document.getElementById("myform").submit();
        }
jQuery(document).ready(function($){
	var t=1;
	var rows = $("input#count_r").val();
	$("#right").live('click', function() {
		if(t ==rows) 
		{ 
			$("#right").removeAttr('href');
			$("#right").removeClass("right");
		}
		else
		{
			$("#right").show();
			$("#left").show();
			$("#left").addClass("left");
			$("#left").attr("href", "javascript:void(0);");	
		}
		if(t ==0) t =1;
		if(t < rows){
			$("#t"+t).fadeOut('slow', function() {
				$("#t"+t).hide();
				t = parseInt(t)+1;		
				$("#t"+t).show();
			 });
		}
	});
	$("#left").live('click', function() {
		if(t ==1) 
		{
			$("#left").hide();
			$("#right").removeClass("left");
		}
		else
		{
			$("#left").show();
			$("#right").show();			
			$("#right").addClass("right");
			$("#right").attr("href", "javascript:void(0);");	
		}
		if(t ==0) {t =1;}
		if(t <= rows && t !=1){
		t = parseInt(t)-1;
			$("#t"+t).fadeIn('slow', function() {
				$("#t"+parseInt(t)+1).hide();				
				$("#t"+t).show();
			 });}});
});
</script>
<?php if(!$data['ftrue']){?>
<style type="text/css">
#header {
background:<?php echo $data['hbg'];?>;}
#top-bg {
background:<?php echo $data['sbg']; ?> url('<?php echo $data['background_s']; ?>') repeat;}
#second-strip {background:<?php echo $data['sbg2']; ?> url('<?php echo $data['background_s2']; ?>') repeat;}
#container { background:<?php echo $data['cbg'];?> url('<?php echo $data['background_c']; ?>') repeat;}
.ddsmoothmenu ul li ul {
background:<?php echo $data['sbg'];?> url('<?php echo $data['background_s']; ?>') repeat;
border-top: solid 2px <?php echo $data['linkcolor'];?>;}
#footer {
background:<?php echo $data['fbg'];?> url('<?php echo $data['background_f']; ?>') repeat;}
#nav {border-bottom: solid 2px <?php echo $data['linkcolor'];?>;}
.ddsmoothmenu ul li a:hover, .ddsmoothmenu ul li a.selected , .current-menu-item  { background:<?php echo $data['linkcolor'];?>;}
#meetrow .block .con-meet p strong, #profile-con .pro-list .block .con-meet p a, #testi-row .testimonial .testi-owner p strong, #profile-con .pro-detail h4, #banner-main-2 .breadcrum ul li span, #banner-main-2 .breadcrum ul li, #profile-con .pro-detail .books-list h3, #blog .section .date-box p span, #content .rightside ul li a:hover, #footer .fwidget ul li a:hover {color:<?php echo $data['linkcolor'];?>;}
.section p a, #rightbar p a, .logged-in-as a, h6 a:hover, a[rel~="category"], .ddsmoothmenu ul li ul li a:hover {color:<?php echo $data['linkcolor'];?>!important;}
</style>
<?php
}?> 
</head>
<body <?php body_class(); ?> onload="initialize()">
<div id="header">
    	<div class="header">
        	<div class="logo"><a href="<?php echo home_url(); ?>/"><img src="<?php echo $data['logo'];?>" border="0" alt="" /></a><label style="font-family: 'DroidSansRegular';color:#a8a6a5;text-transform:uppercase;display:block;font-size:11px"><?php echo $data['logoline'];?></label></div>
           <div class="phone"><h5><?php echo $data['phone']; ?></h5><p><?php echo $data['phone2']; ?></p></div>
        </div>
    </div>	
	<div id="top-bg">
    	<div id="nav">
                <?php wp_nav_menu(array( 'container_class' => 'ddsmoothmenu', 'container_id' => 'smoothmenu1', 'theme_location' => 'primary') ); ?>
        </div>   