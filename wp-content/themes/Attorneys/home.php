<?php
/*
Template Name: Home  Template
*/
get_header();
global $data; ?>
 <div id="banner-main">
        	<div class="indiv">
        	<div class="slider">            	
                <div id="slider">
                	<ul>
                     <?php
						$allslides = $data['example_slider'];
                        foreach ($allslides as $slides) {
                        ?>
                    	<li>
                        	<div class="image">
							<?php
							if($slides['link']!=""){ echo '<a href="'.$slides['link'].'">';}?>
							<img src="<?php echo $slides['url'] ?>" border="0" alt="" />
							<?php if($slides['link']!=""){ echo '</a>';}?>
							</div>
                            <div class="caption">
                                <h2>
								<?php if($slides['link']!=""){ echo '<a href="'.$slides['link'].'">';}?>
								<span><?php echo $slides['title']; ?></span>
								<?php if($slides['link']!=""){ echo '</a>';}?>
								</h2>
                                <?php echo $slides['description'];?>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            </div>
        </div>
        <div id="second-strip">
        	<div class="strip">
            	<div class="cta-btn"><a href="/contact" class="smcf-link"><img src="/wp-content/uploads/contact_us.png"></a></div>
            	<p><?php echo $data['tagline']; ?></p>
            </div>
        </div>
		<div id="container">
        	<div id="content">
            	<div class="leftside">
				<h3 class="widget-title">About Us</h3>
				<?php
$currentpage = $data['homepage_page'];
$pageset = shopping_pages($currentpage);
$or = get_page($pageset);?>
                    <div class="image-left"><?php echo get_the_post_thumbnail($or->ID,'home');?></div>
                    <?php
$cont = apply_filters('the_content', $or->post_content);echo $cont;?>
				</div>
                <div class="rightside">
                	<h2>Areas of <strong>Practice</strong></h2>
                    <ul>
                    	<li><a href="/practice-areas#slipandfall">Slip & Fall</a></li>
                        <li><a href="/practice-areas#ttc">Public Transportation</a></li>
                        <li><a href="/practice-areas#court">Small Claims Court</a></li>
                        <li><a href="/practice-areas#tickets">Traffic Tickets</a></li>
                        <li><a href="/practice-areas#mvas">Motor Vehicle Accidents</a></li>
                        <li><a href="/practice-areas#notary">Commissioner of Oaths & Notary Public Services</a></li>
                    </ul>
                </div>
<!---
                <div id="meetrow">
                	<h3>Meet the <strong>Attorneys</strong></h3>
                    <?php
					$atorns = new WP_Query("post_type=attorneys");
					$count_records = 0;
					while($atorns->have_posts()):$atorns->the_post();
					$count_records++;
					if($count_records <=3)
					{
						$class = '';
						if($count_records == 3)
						{
							$class = 'block last';
						}
						else
						{
							$class = 'block';
						} ?>

                    <div class="<?php echo $class;?>">
                    	<div class="image"><a href="<?php the_permalink();?>"><?php the_post_thumbnail('attorney');?></a> </div>
                        <div class="con-meet">
                        	<p style="padding-bottom:0px"><strong><a href="<?php the_permalink();?>"><?php the_title();?></a></strong></p><p style="padding-top:5px"><?php $caa2 = get_the_excerpt();
echo substr($caa2, 0, 168);?></p>
                        </div>
                    </div>
					<?php } endwhile; ?>
                </div>
-->
 <div id="testi-row">
 					<?php if ( !$data['testitrue'] ):?>	
                	<div class="testimonial">
                    	<h3>Testimonials<div class="leftright"><a  id="left" href="javascript:void(0)" class="left"></a><a id="right" href="javascript:void(0)" class="right"></a></div></h3>
                        <div class="divt">
                         <?php
						$testis = new WP_Query("post_type=testimonials");
												$count_row	= 0;	
						echo ' <input type="hidden" value="'.$testis->found_posts.'" id="count_r" name="count_r" />';
						while($testis->have_posts()):$testis->the_post();	
						$count_row++;
						?>
                        <div id="t<?php echo $count_row;?>" class="record">				
    						<div class="testi-text">
                            	<p><?php the_content(); ?> 
                                <div class="testi-owner">
                                    <p style="float:right"><?php
                                    $awe = get_post_custom($testis->ID);
                                    echo $awe[dbt_designation][0];
                                    ?>
                                    </p>
                                </div>
                            </p>
                            </div>
						</div><?php endwhile; ?>
		</div>
	</div>			<?php endif; ?>
    				<?php if ( !$data['awardstrue'] ):?>						               
                    <div class="awards">
                    	<h3>Awards &amp; <strong>Accomplishments</strong></h3>
                        <?php
                        $allslides = $data['example_slider2'];
?>
                        <div class="con-award">
                        <?php 
						foreach ($allslides as $slide) {
						
						?>
                        <div class="fat"><?php
                        if($slide['link']!=""){ echo '<a href="';echo $slide['link'];echo '">';}?>
                        <img src="<?php	 echo $slide['url'];?>" border="0" alt="" /><?php if($slide['link']!=""){ echo '</a>';}?></div>
                        <?php } ?>
						</div>
						</div>
                        <?php endif; ?>
						</div>
						</div>
						</div>
						</div>
<?php get_footer(); ?>
