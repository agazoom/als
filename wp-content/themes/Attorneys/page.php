<?php
get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<div id="banner-main-2">
        	<div class="indiv">
                <div class="slider">
                    <h2><?php the_title()?></h2>
                    <p><?php echo get_the_excerpt();?></p>
                    <div class="breadcrum">
                        <ul>
                            <li><?php the_breadcrumb()?></li>
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="container">
        	<div id="content">
            	
                <div id="mid-content">
                  <?php the_content(); ?>
                </div>
                
            </div>
        </div>
        
    
<?php endwhile; // end of the loop. ?>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>